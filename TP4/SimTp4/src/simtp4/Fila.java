/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp4;

/**
 *
 * @author NICO
 */
public class Fila {
    private int number;
    private String rnd1;
    private Integer primerTiro;
    private String rnd2;
    private Integer segundoTiro;
    private Integer puntos;
    private Integer rondasGanadas;
    private Integer rondas;
    private Integer partida;

    public Fila() {
    }

    public Fila(int number, String rnd1, Integer primerTiro, String rnd2, Integer segundoTiro, Integer puntos, Integer rondasGanadas, Integer rondas,Integer partida) {
        this.number = number;
        this.rnd1 = rnd1;
        this.primerTiro = primerTiro;
        this.rnd2 = rnd2;
        this.segundoTiro = segundoTiro;
        this.puntos = puntos;
        this.rondasGanadas = rondasGanadas;
        this.rondas = rondas;
        this.partida = partida;
    }

    public Integer getPartida() {
        return partida;
    }

    public void setPartida(Integer partida) {
        this.partida = partida;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getRnd1() {
        return rnd1;
    }

    public void setRnd1(String rnd1) {
        this.rnd1 = rnd1;
    }

    public Integer getPrimerTiro() {
        return primerTiro;
    }

    public void setPrimerTiro(Integer primerTiro) {
        this.primerTiro = primerTiro;
    }

    public String getRnd2() {
        return rnd2;
    }

    public void setRnd2(String rnd2) {
        this.rnd2 = rnd2;
    }

    public Integer getSegundoTiro() {
        return segundoTiro;
    }

    public void setSegundoTiro(Integer segundoTiro) {
        this.segundoTiro = segundoTiro;
    }

    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }

    public Integer getRondasGanadas() {
        return rondasGanadas;
    }

    public void setRondasGanadas(Integer rondasGanadas) {
        this.rondasGanadas = rondasGanadas;
    }

    public Integer getRondas() {
        return rondas;
    }

    public void setRondas(Integer rondas) {
        this.rondas = rondas;
    }
    
    public void addPuntos(Integer pts){
        puntos+=pts;
    } 
    
     public void addRondasGanadas(Integer rg){
        rondasGanadas+=rg;
    } 
    
      public void addRondas(Integer r){
        rondas+=r;
    }
     
    public double probabilidad(){
        return ((double) rondasGanadas/rondas);
    }  
}
