/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp3;

/**
 *
 * @author Juan
 */
public class Frecuencia {
    
    public int valor;
    public int frecObs;
    public double prob;
    public int frecEsp;
    
    
    public Frecuencia(int val, int fo, double pro, int fe){
        
        valor = val;
        frecObs = fo;
        frecEsp = fe;
        prob = pro;
        
        
    }

    public int getValor() {
        return valor;
    }

    public int getFrecObs() {
        return frecObs;
    }

    public int getFrecEsp() {
        return frecEsp;
    }
    
    
    
    
}
