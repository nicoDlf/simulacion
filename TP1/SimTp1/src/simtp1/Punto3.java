
package simtp1;


import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class Punto3 extends javax.swing.JFrame {
    //ATRIBUROS
    int cantIntervalos = 0;
    int semilla = 0;
    int cteMult = 0;
    int cteAditiva = 0;
    int valorMod = 0;
    ArrayList<String> numsAlet = new ArrayList<String>();
    ArrayList<Double> sumParaChi2 = new ArrayList<Double>();
    int cantNum = 0;
    int frecEsp;
    int grados= 0;
    //Determinar el numero de decimales despues de la coma
    DecimalFormat df = new DecimalFormat("#.##");
    
    ArrayList<String> intervalosParaExcel = new ArrayList<String>();
    ArrayList<Integer> frecuenciaEsperadaExc = new ArrayList<Integer>();
    ArrayList<Integer> frecuenciaObservadaExc = new ArrayList<Integer>();
    ArrayList<Double> marcaDeClase = new ArrayList<Double>();
    
    //METODOS
    public Punto3() {
        initComponents();//Inicializa los componentes en la interfaz. (Los muestra).
        this.setLocationRelativeTo(null);//Hace que la ventana se vea en el centro de la pantalla.
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpCantInt = new javax.swing.ButtonGroup();
        lblTitulo = new java.awt.Label();
        lblSemilla = new java.awt.Label();
        lblCteMultiplicativa = new java.awt.Label();
        lblCteAditiva = new java.awt.Label();
        lblValorModulo = new java.awt.Label();
        lblCantIntervalos = new java.awt.Label();
        txtSemilla = new java.awt.TextField();
        txtCteMult = new java.awt.TextField();
        txtCtAditiva = new java.awt.TextField();
        txtVModulo = new java.awt.TextField();
        rbttn5 = new javax.swing.JRadioButton();
        rbttn10 = new javax.swing.JRadioButton();
        rbttb15 = new javax.swing.JRadioButton();
        rbttn20 = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableSeries = new javax.swing.JTable();
        bttnGenerar = new java.awt.Button();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableFrecuencias = new javax.swing.JTable();
        bttnExportarExcel = new java.awt.Button();
        bttnSalir = new java.awt.Button();
        bttnVolver = new java.awt.Button();
        lblResultado = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblXtabulado = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblX2calculado = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblError = new javax.swing.JLabel();
        btnGraficar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        lblTitulo.setText("Prueba Chi-Cuadrad con Método Lineal");

        lblSemilla.setText("Semilla (Xo):");

        lblCteMultiplicativa.setText("Cte. (k) -  Cte. Mult.:");

        lblCteAditiva.setText("Cte. Aditiva (c):");

        lblValorModulo.setText("Cte. (g) - Valor modulo : ");

        lblCantIntervalos.setText("Cantidad de Intervalos:");

        txtSemilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSemillaActionPerformed(evt);
            }
        });
        txtSemilla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSemillaKeyTyped(evt);
            }
        });

        txtCteMult.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCteMultKeyTyped(evt);
            }
        });

        txtCtAditiva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCtAditivaKeyTyped(evt);
            }
        });

        txtVModulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVModuloKeyTyped(evt);
            }
        });

        grpCantInt.add(rbttn5);
        rbttn5.setText("5");
        rbttn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn5ActionPerformed(evt);
            }
        });

        grpCantInt.add(rbttn10);
        rbttn10.setText("10");
        rbttn10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn10ActionPerformed(evt);
            }
        });

        grpCantInt.add(rbttb15);
        rbttb15.setText("15");
        rbttb15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttb15ActionPerformed(evt);
            }
        });

        grpCantInt.add(rbttn20);
        rbttn20.setText("20");
        rbttn20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn20ActionPerformed(evt);
            }
        });

        jTableSeries.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Series"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableSeries);

        bttnGenerar.setLabel("Generar números");
        bttnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnGenerarActionPerformed(evt);
            }
        });

        jTableFrecuencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Intervalos", "fo", "fe", "A= (fe-fo)**2", "A/fe"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableFrecuencias);

        bttnExportarExcel.setLabel("Exportar a Excel");
        bttnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnExportarExcelActionPerformed(evt);
            }
        });

        bttnSalir.setBackground(new java.awt.Color(0, 0, 255));
        bttnSalir.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        bttnSalir.setForeground(new java.awt.Color(255, 255, 255));
        bttnSalir.setLabel("Salir");
        bttnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSalirActionPerformed(evt);
            }
        });

        bttnVolver.setBackground(new java.awt.Color(0, 0, 255));
        bttnVolver.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        bttnVolver.setForeground(new java.awt.Color(255, 255, 255));
        bttnVolver.setLabel("Volver");
        bttnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnVolverActionPerformed(evt);
            }
        });

        jLabel4.setText("Resultado:");

        jLabel3.setText("X^2 tabulado: ");

        jLabel1.setText("X^2 calculado: ");

        btnGraficar.setText("Graficar");
        btnGraficar.setEnabled(false);
        btnGraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGraficarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 489, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCteAditiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCteMultiplicativa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bttnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCteMult, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCtAditiva, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(62, 62, 62)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblValorModulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblCantIntervalos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtVModulo, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rbttb15)
                                            .addComponent(rbttn5))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rbttn20)
                                            .addComponent(rbttn10))))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bttnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(303, 303, 303)
                        .addComponent(btnGraficar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bttnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblError, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(bttnGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 489, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblX2calculado, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblXtabulado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bttnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bttnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVModulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblValorModulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(lblCteMultiplicativa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCteMult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rbttn5)
                                .addComponent(rbttn10))))
                    .addComponent(lblCantIntervalos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCteAditiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCtAditiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbttb15)
                        .addComponent(rbttn20)))
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bttnGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1, Short.MAX_VALUE))
                    .addComponent(lblError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblXtabulado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(jLabel3)))
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lblResultado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblX2calculado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bttnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGraficar))
                .addContainerGap())
        );

        lblCteMultiplicativa.getAccessibleContext().setAccessibleName("Cte. (k) - Cte. Multiplicativa:");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSemillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSemillaActionPerformed
                /*
        Este método recibe la acción del ingreso de la cantidad de muestras.
        */
    }//GEN-LAST:event_txtSemillaActionPerformed

    private void bttnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSalirActionPerformed
       
        System.exit(0);//Este método está asociado al botón salir. Finaliza la ejecución del programa.
    }//GEN-LAST:event_bttnSalirActionPerformed

    private void bttnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnVolverActionPerformed
         /*
        Con este método vuelvo al menú principal al presionar el botón "Volver"
        */
        MenuPrincipal volver = new MenuPrincipal();
        volver.setVisible(true);
        close();//metodo para cerrar ventana y que no se clone 
    }//GEN-LAST:event_bttnVolverActionPerformed

    private void rbttb15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttb15ActionPerformed
       
        cantIntervalos = 15; //Al seleccionar el 5, establezco que cantIntervalos valga lo especificado.
    }//GEN-LAST:event_rbttb15ActionPerformed

    private void rbttn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn5ActionPerformed
        
        cantIntervalos = 5;        
    }//GEN-LAST:event_rbttn5ActionPerformed

    private void rbttn10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn10ActionPerformed
       
        cantIntervalos = 10;
    }//GEN-LAST:event_rbttn10ActionPerformed

    private void rbttn20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn20ActionPerformed
        
        cantIntervalos = 20;
    }//GEN-LAST:event_rbttn20ActionPerformed

    private void bttnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnGenerarActionPerformed
       
        /*En estas variables consecutivas lo que se hace es "limpiar" los datos ingresados anteriormente
        por cada "Generar números" que se presione.
        */
        cantNum = 0;
        numsAlet.clear();
        intervalosParaExcel.clear();
        frecuenciaObservadaExc.clear();
        frecuenciaEsperadaExc.clear();
        marcaDeClase.clear();
        btnGraficar.setEnabled(true);
        this.metodoLineal(); //llamo a métodoLineal() para agregar valores a numsAlet
        
         //En model lo que se le asigna es el JTable (El cudro de datos de la interaz) para luego colocar los datos correspontdientes 
        DefaultTableModel model = (DefaultTableModel) jTableSeries.getModel();
        model.setRowCount(0);//borro los datos qe la tabla tenía anteriormente
        
        String rowData[] = new String [1];//Creo una lista de String con una columna 
        
        /*
        Con el for voy a añadiendo a la lista rowData[] cada valor que contiene
        el vecto numsAlet y los agrego a la tabla de numeros aleatorios.
        */
        for (int i = 0; i < numsAlet.size(); i++) {
            rowData[0] = numsAlet.get(i);
            model.addRow(rowData);
            
            
        }  
        
        this.obtenerFrecuencia();//Llamo al método para realizar la tabla de frecuencia
        
        
    }//GEN-LAST:event_bttnGenerarActionPerformed

    private void bttnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnExportarExcelActionPerformed
      
        Workbook libro = new XSSFWorkbook();//creo un libro excel xlxs
        Sheet hoja = libro.createSheet("Tabla de frecuencias");//le pongo nombre al libro de excel
        
        Row fila1 = hoja.createRow(0);//le pongo nombre al libro de excel
        fila1.createCell(0).setCellValue("Intervalo");//en la celda 1 de la fila 1 coloco "Intervalo"
        fila1.createCell(1).setCellValue("Fo");//idem anterior igual que el resto
        fila1.createCell(2).setCellValue("Fe");
        fila1.createCell(3).setCellValue("Marca de clase");
        
          /*
        Mediante este for creo filas y coloco los datos obetnidos en las columnas correspondientes.
        */
        for (int i = 0; i < frecuenciaEsperadaExc.size(); i++) {
            Row filaExc = hoja.createRow(i+1);
            filaExc.createCell(0).setCellValue(intervalosParaExcel.get(i));
            filaExc.createCell(1).setCellValue(frecuenciaObservadaExc.get(i));
            filaExc.createCell(2).setCellValue(frecuenciaEsperadaExc.get(i));
            filaExc.createCell(3).setCellValue(marcaDeClase.get(i));
            
        }
        try {
            //Al finalizar se crea el archivo excel con el nombre ExcelPunto2
            FileOutputStream archivo1 = new  FileOutputStream("ExcelPunto3.xlsx");
            libro.write(archivo1);
            archivo1.close();
        //Detecta posibles errores
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Punto2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Punto2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_bttnExportarExcelActionPerformed

    private void txtSemillaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSemillaKeyTyped
        /*
        Con este método evito que se ingrese letras en vez de números en donde corresponden
        */
        char tecla = evt.getKeyChar();
        if (tecla < '0' || tecla > '9') {
		getToolkit().beep();
		evt.consume();
	}
    }//GEN-LAST:event_txtSemillaKeyTyped

    private void txtCteMultKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCteMultKeyTyped
        
        char tecla = evt.getKeyChar();
        if (tecla < '0' || tecla > '9') {
		getToolkit().beep();
		evt.consume();
	}
    }//GEN-LAST:event_txtCteMultKeyTyped

    private void txtCtAditivaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCtAditivaKeyTyped
        
        char tecla = evt.getKeyChar();
        if (tecla < '0' || tecla > '9') {
		getToolkit().beep();
		evt.consume();
	}
    }//GEN-LAST:event_txtCtAditivaKeyTyped

    private void txtVModuloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVModuloKeyTyped
        
        char tecla = evt.getKeyChar();
        if (tecla < '0' || tecla > '9') {
		getToolkit().beep();
		evt.consume();
	}
    }//GEN-LAST:event_txtVModuloKeyTyped

    private void btnGraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficarActionPerformed
        // TODO add your handling code here:
        DefaultCategoryDataset data = new DefaultCategoryDataset();
        for (int i = 0; i < frecuenciaEsperadaExc.size(); i++) {
            data.addValue(frecuenciaObservadaExc.get(i), intervalosParaExcel.get(i),"frecuencia observada");
            data.addValue(frecuenciaEsperadaExc.get(i), intervalosParaExcel.get(i),"frecuencia esperada: "+ frecuenciaEsperadaExc.get(i));

        }
        

        JFreeChart grafica = ChartFactory.createBarChart("Grafica Frecuencia" , "Intervalos", "Frecuencias", data, PlotOrientation.VERTICAL, true,true,false);
        
        ChartPanel contenedor= new ChartPanel(grafica);
     
        
        JFrame ventana = new JFrame("Grafica");
        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ventana.add(contenedor);
        ventana.setSize(800,400);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(this);
    }//GEN-LAST:event_btnGraficarActionPerformed
    
    private void metodoLineal(){
        //Asigno a cada variable losvalores correspondientes
        int cteK = 0;
        int cteG = 0;
        semilla = Integer.parseInt(txtSemilla.getText().toString());//el método getText trae el valor escrito en el textbox
        cteK = Integer.parseInt(txtCteMult.getText().toString());
        cteAditiva = Integer.parseInt(txtCtAditiva.getText().toString());
        cteG = Integer.parseInt(txtVModulo.getText().toString());
        
        //para cumplir la regla  del maximo periodo 
        cteMult = 1 + 4* cteK;
        valorMod =(int) Math.pow(2, cteG);
        
        //Calculo Xo, Xi+1 y ri
        int xi = semilla;
        int xi1Inicial = (cteMult * xi + cteAditiva) % valorMod;
        int xi1Final = 0;
        xi = xi1Inicial;
        double ri = (double) xi1Inicial / (valorMod-1);
        
        //hasta que se repita el primer valor 
        while(xi1Inicial != xi1Final ){
            numsAlet.add(String.valueOf(df.format(ri)));
            xi1Final = (cteMult * xi + cteAditiva) % valorMod;
            xi = xi1Final;
            ri = (double)xi1Final / (valorMod-1);
            
        }  
        
        cantNum = numsAlet.size();
        
    }
    public void obtenerFrecuencia(){
        /*
        Inicializo y asigno los valoes correspondientes  cada variable
        */
        double intervalo = (double)1/cantIntervalos;
        double limisteInf = 0.0;
        int frecObs = 0;
        double limiteSup = intervalo;
        double res= 0.0;
        frecEsp = cantNum/cantIntervalos;
        grados = cantIntervalos - 1 ;
        sumParaChi2 = null;
        sumParaChi2 = new ArrayList<>();
        
         //Si la frecuendia esp es mayor a 5...
        if(frecEsp >=5){
            
        //Asigno a model la tabla de frecuencias
        DefaultTableModel model = (DefaultTableModel) jTableFrecuencias.getModel();
        String fila[] = new String[5];//Creo una lista de String 5 columnas
        model.setRowCount(cantIntervalos);//La cantidad de filas de la tabla va a ser igual a la cantidad de intervalos seleccionado
        model.setColumnCount(5);//la cantiad de columnas de la tabla va a ser igual a 5
        
        ArrayList <String> inters = new ArrayList<String>();//Creo vector inters de String (intervalos)
        
        String matriz[][] = new String[cantIntervalos][5];//Creo una matriz de String el cual se visualizará en la tabla
        
        //Doy tantas iteraciones como intervalos tenga
        for (int i = 0; i < cantIntervalos; i++) {
            
            //Posiciono en la primera columna los intervalos
            matriz[i][0] = df.format(limisteInf) + " - " + df.format(limiteSup);
            model.setValueAt(matriz[i][0], i, 0);
            
            intervalosParaExcel.add(matriz[i][0]);//Agrego a la lista el valor de la posición i j de la matriz
            //Doy tantas vueltas como cantidad de números aleatorio posea
            for (int j = 0; j < numsAlet.size(); j++) {
                
                double valor = Double.parseDouble(numsAlet.get(j).replace(",", "."));//Asigno a valor un numAlet j
                //Con este if verifico que el valor se encuentre denro de los limites para contabilizar las frecuncias observadas
                if(valor >= limisteInf && valor < limiteSup){
                    frecObs += 1;
                }
                
      
            }
            
            frecuenciaObservadaExc.add(frecObs);//Agrego a la lista la cantidad de frec obs.
            matriz[i][1] = String.valueOf(frecObs);//Asigno el valor de la frec obs en la posicion i j de la matriz
            frecuenciaEsperadaExc.add(frecEsp);//Agrego a la lista frec obs.
            matriz[i][2] = String.valueOf(frecEsp);//Asigno el valor de la frec esp en la posicion i j de la matriz
            matriz[i][3] = String.valueOf(Math.pow((frecEsp - frecObs), 2));//misma procedimiento que la enterior
            res=Math.pow((frecEsp - frecObs), 2)/frecEsp;
            matriz[i][4] =df.format(res);//idem
            sumParaChi2.add(res);
            marcaDeClase.add((limisteInf + limiteSup)/2);
            
            //Muestro en la tabla los valores correspondientes  de cada columna
            model.setValueAt(matriz[i][1], i, 1);
            model.setValueAt(matriz[i][2], i, 2);
            model.setValueAt(matriz[i][3], i, 3);
            model.setValueAt(matriz[i][4], i, 4);
           
            //Cambio al siguiente inervalo
            limisteInf = limiteSup;
            limiteSup = limiteSup + intervalo;
            frecObs = 0;//Vuelvo a poner en cero la frec Obs para realizar nuevamentela operación
            
        }
        
        pruebaChi2();//Realizo la prueba Chi Cuadrado
        
        }else{
            lblError.setText("Frec. Esperada debe ser mayor a 5");
        }
    }
    
        private void pruebaChi2() {
            
        //Realizo la sumatoria Chicuadrado
        double chi2calculado=0.0;
        double chi2tabulado =0.0;
        String res = "Rechaza";
        
        for (int i = 0; i < sumParaChi2.size(); i++) {
             chi2calculado+= sumParaChi2.get(i);
            
        }
         //Obtengo el Chi2 tabulado de acuerdo a los grados de livertad calculado
        switch(grados){
            case 4:
                chi2tabulado = 9.49;
                break;
            case 9:
                 chi2tabulado = 16.9;
                break;
            case 14:
                 chi2tabulado = 23.7;
                break;
            case 19:
                 chi2tabulado = 30.1;
                break;
            default:
                
        }
        //Compruebo si se acepta o no la hipótesis
        if(chi2calculado <= chi2tabulado ){
            res = "Acepta";
        }
        
        lblX2calculado.setText(df.format(chi2calculado));//Muestro el chi2 calculado
        lblXtabulado.setText(String.valueOf(chi2tabulado));//Muestro el chi2 Tabulado
        lblResultado.setText(res);//Muestro el resultado de la hipótesis

        
    }
    
     
     public void close(){
        WindowEvent winClosingEvent = new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Punto3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Punto3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Punto3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Punto3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Punto3().setVisible(true);
            }
        });
    }
    
    //ATRIBUTOS DE LA INTERFAZ
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGraficar;
    private java.awt.Button bttnExportarExcel;
    private java.awt.Button bttnGenerar;
    private java.awt.Button bttnSalir;
    private java.awt.Button bttnVolver;
    private javax.swing.ButtonGroup grpCantInt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableFrecuencias;
    private javax.swing.JTable jTableSeries;
    private java.awt.Label lblCantIntervalos;
    private java.awt.Label lblCteAditiva;
    private java.awt.Label lblCteMultiplicativa;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblResultado;
    private java.awt.Label lblSemilla;
    private java.awt.Label lblTitulo;
    private java.awt.Label lblValorModulo;
    private javax.swing.JLabel lblX2calculado;
    private javax.swing.JLabel lblXtabulado;
    private javax.swing.JRadioButton rbttb15;
    private javax.swing.JRadioButton rbttn10;
    private javax.swing.JRadioButton rbttn20;
    private javax.swing.JRadioButton rbttn5;
    private java.awt.TextField txtCtAditiva;
    private java.awt.TextField txtCteMult;
    private java.awt.TextField txtSemilla;
    private java.awt.TextField txtVModulo;
    // End of variables declaration//GEN-END:variables
}
