
package simtp1;


import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.jfree.chart.*;
import org.jfree.data.category.*;
import org.jfree.chart.plot.*;

public class Punto2 extends javax.swing.JFrame {
    
    //ATRIBUROS
    int cantIntervalos = 0;
    int cantNums = 0;
    ArrayList<Double> numsAleat = new ArrayList<>();
    DecimalFormat df = new DecimalFormat("#.##");
    int frecEsp;
    int grados= 0;
    
    ArrayList<String> intervalosParaExcel = new ArrayList<String>();
    ArrayList<Integer> frecuenciaEsperadaExc = new ArrayList<Integer>();
    ArrayList<Integer> frecuenciaObservadaExc = new ArrayList<Integer>();
    ArrayList<Double> marcaDeClase = new ArrayList<Double>();
    ArrayList<Double> sumParaChi2 = new ArrayList<Double>();
    
    //METODOS
    public Punto2() {
        initComponents(); //Inicializa los componentes en la interfaz. (Los muestra).
        this.setLocationRelativeTo(null); //Hace que la ventana se vea en el centro de la pantalla.
        
        
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupInter = new javax.swing.ButtonGroup();
        jlblNomVentana = new javax.swing.JLabel();
        jlblCanN = new javax.swing.JLabel();
        jlblCantT = new javax.swing.JLabel();
        jtxtCantN = new javax.swing.JTextField();
        salir = new java.awt.Button();
        jScrollPane1 = new javax.swing.JScrollPane();
        num = new javax.swing.JTable();
        rbttn5 = new javax.swing.JRadioButton();
        rbttn10 = new javax.swing.JRadioButton();
        rbttn15 = new javax.swing.JRadioButton();
        rbttn20 = new javax.swing.JRadioButton();
        bttnGenerarNums = new java.awt.Button();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaFrec = new javax.swing.JTable();
        bttnExcel = new java.awt.Button();
        bttnVolverAtras = new java.awt.Button();
        jLabel1 = new javax.swing.JLabel();
        lblX2calculado = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblXtabulado = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblResultado = new javax.swing.JLabel();
        lblError = new javax.swing.JLabel();
        btnGraficar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jlblNomVentana.setText("Prueba Chi Cuadrado");

        jlblCanN.setText("Cantidad de números aleatorios (N):");

        jlblCantT.setText("Cantidad de intervalos (T):");

        jtxtCantN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtCantNActionPerformed(evt);
            }
        });
        jtxtCantN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtxtCantNKeyTyped(evt);
            }
        });

        salir.setActionCommand("Salir");
        salir.setBackground(new java.awt.Color(0, 0, 255));
        salir.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        salir.setForeground(new java.awt.Color(255, 255, 255));
        salir.setLabel("Salir");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });

        num.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Series"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        num.setToolTipText("");
        jScrollPane1.setViewportView(num);

        groupInter.add(rbttn5);
        rbttn5.setText("5");
        rbttn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn5ActionPerformed(evt);
            }
        });

        groupInter.add(rbttn10);
        rbttn10.setText("10");
        rbttn10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn10ActionPerformed(evt);
            }
        });

        groupInter.add(rbttn15);
        rbttn15.setText("15");
        rbttn15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn15ActionPerformed(evt);
            }
        });

        groupInter.add(rbttn20);
        rbttn20.setText("20");
        rbttn20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbttn20ActionPerformed(evt);
            }
        });

        bttnGenerarNums.setLabel("Generar números");
        bttnGenerarNums.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnGenerarNumsActionPerformed(evt);
            }
        });

        tablaFrec.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Intervalo", "fo", "fe", "A= (fe - fo)**2", "A/fe"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tablaFrec);

        bttnExcel.setLabel("Exportar a Excel");
        bttnExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnExcelActionPerformed(evt);
            }
        });

        bttnVolverAtras.setBackground(new java.awt.Color(0, 0, 255));
        bttnVolverAtras.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        bttnVolverAtras.setForeground(new java.awt.Color(255, 255, 255));
        bttnVolverAtras.setLabel("Volver");
        bttnVolverAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnVolverAtrasActionPerformed(evt);
            }
        });

        jLabel1.setText("X^2 calculado: ");

        jLabel2.setText("<=");

        jLabel3.setText("X^2 tabulado: ");

        jLabel4.setText("Resultado:");

        btnGraficar.setText("Graficar");
        btnGraficar.setEnabled(false);
        btnGraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGraficarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblX2calculado, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblXtabulado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(bttnVolverAtras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jlblNomVentana)
                                .addGap(177, 177, 177)
                                .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jlblCanN)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jtxtCantN, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jlblCantT)
                                        .addGap(41, 41, 41)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(rbttn10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(rbttn20))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(rbttn5)
                                                .addGap(33, 33, 33)
                                                .addComponent(rbttn15)))))
                                .addGap(0, 267, Short.MAX_VALUE))
                            .addComponent(jScrollPane2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(lblError, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bttnGenerarNums, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGraficar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bttnExcel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnGraficar)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlblNomVentana)
                            .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bttnVolverAtras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlblCanN)
                            .addComponent(jtxtCantN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlblCantT)
                            .addComponent(rbttn5)
                            .addComponent(rbttn15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbttn10)
                            .addComponent(rbttn20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bttnGenerarNums, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblXtabulado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)))
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblResultado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(213, 213, 213)
                                .addComponent(lblX2calculado, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bttnExcel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtCantNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtCantNActionPerformed
        
        /*
        Este método recibe la acción del ingreso de la cantidad de muestras.
        */
    }//GEN-LAST:event_jtxtCantNActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        
        System.exit(0); //Este método está asociado al botón salir. Finaliza la ejecución del programa.
    }//GEN-LAST:event_salirActionPerformed

    private void rbttn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn5ActionPerformed
        
        cantIntervalos = 5; //Al seleccionar el 5, establezco que cantIntervalos valga lo especificado.
        
        
    }//GEN-LAST:event_rbttn5ActionPerformed

    private void rbttn10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn10ActionPerformed
        
        cantIntervalos = 10;
       
    }//GEN-LAST:event_rbttn10ActionPerformed

    private void rbttn15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn15ActionPerformed
        
        cantIntervalos = 15;
        
    }//GEN-LAST:event_rbttn15ActionPerformed

    private void rbttn20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbttn20ActionPerformed
        
        cantIntervalos = 20;
        
    }//GEN-LAST:event_rbttn20ActionPerformed

    private void bttnGenerarNumsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnGenerarNumsActionPerformed
        
        /*En estas variables consecutivas lo que se hace es "limpiar" los datos ingresados anteriormente
        por cada "Generar números" que se presione.
        */
        cantNums = 0; 
        numsAleat.clear();
        intervalosParaExcel.clear();
        frecuenciaObservadaExc.clear();
        frecuenciaEsperadaExc.clear();
        marcaDeClase.clear();
        btnGraficar.setEnabled(true);
        
        //Luego de limpiar asigno el nuevo valor a cantNums
        cantNums = Integer.parseInt(jtxtCantN.getText().toString());
              
      
        //En model lo que se le asigna es el JTable (El cudro de datos de la interaz) para luego colocar los datos correspontdientes 
        DefaultTableModel model = (DefaultTableModel) num.getModel();
        model.setRowCount(0);//borro los datos qe la tabla tenía anteriormente
        
        /*
        Con el for voy añadiendo al vector numsAleat los valores 
        generados por el random
        */
        for (int i = 0; i < cantNums; i++) {
            
            numsAleat.add(Math.random());  
        }
        
        
        String rowData[] = new String [1];//Creo una lista de String
        
        /*
        Con el for voy a añadiendo a la lista rowData[] cada valor que contiene
        el vecto numsAlet y los agrego a la tabla de numeros aleatorios.
        */
        for (int i = 0; i < numsAleat.size(); i++) {
            rowData[0] = df.format(numsAleat.get(i));
            model.addRow(rowData);
            
            
        }  
        
        obtenerFrecuencia();//Llamo al método para realizar la tabla de frecuencia
        
    }//GEN-LAST:event_bttnGenerarNumsActionPerformed

    private void bttnExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnExcelActionPerformed
        
        Workbook libro = new XSSFWorkbook();//creo un libro excel xlxs
        Sheet hoja = libro.createSheet("Tabla de frecuencias"); //le pongo nombre al libro de excel
        
        Row fila1 = hoja.createRow(0); //creo fila 1 en excel;
        fila1.createCell(0).setCellValue("Intervalo");//en la celda 1 de la fila 1 coloco "Intervalo"
        fila1.createCell(1).setCellValue("Fo");//idem anterior igual que el resto
        fila1.createCell(2).setCellValue("Fe");
        fila1.createCell(3).setCellValue("Marca de clase");
        
        /*
        Mediante este for creo filas y coloco los datos obetnidos en las columnas correspondientes.
        */
        for (int i = 0; i < frecuenciaEsperadaExc.size(); i++) {
            Row filaExc = hoja.createRow(i+1);
            filaExc.createCell(0).setCellValue(intervalosParaExcel.get(i));
            filaExc.createCell(1).setCellValue(frecuenciaObservadaExc.get(i));
            filaExc.createCell(2).setCellValue(frecuenciaEsperadaExc.get(i));
            filaExc.createCell(3).setCellValue(marcaDeClase.get(i));
            
        }
        try {
            //Al finalizar se crea el archivo excel con el nombre ExcelPunto2
            FileOutputStream archivo = new  FileOutputStream("ExcelPunto2.xlsx");
            libro.write(archivo);
            archivo.close();
        
        //Detecta posibles errores
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Punto2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Punto2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
    }//GEN-LAST:event_bttnExcelActionPerformed

    private void bttnVolverAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnVolverAtrasActionPerformed
        /*
        Con este método vuelvo al menú principal al presionar el botón "Volver"
        */
        MenuPrincipal volver = new MenuPrincipal();
        volver.setVisible(true);
        close(); //Este método cierra la ventana actual
    }//GEN-LAST:event_bttnVolverAtrasActionPerformed

    private void jtxtCantNKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtxtCantNKeyTyped
        /*
        Con este método evito que se ingrese letras en vez de números en donde corresponden
        */
        char tecla = evt.getKeyChar();
        if (tecla < '0' || tecla > '9') {
		getToolkit().beep();
		evt.consume();
	}
    }//GEN-LAST:event_jtxtCantNKeyTyped

    private void btnGraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficarActionPerformed
        // TODO add your handling code here:
        DefaultCategoryDataset data = new DefaultCategoryDataset();
        for (int i = 0; i < frecuenciaEsperadaExc.size(); i++) {
            data.addValue(frecuenciaObservadaExc.get(i), intervalosParaExcel.get(i),"frecuencia observada");
            data.addValue(frecuenciaEsperadaExc.get(i), intervalosParaExcel.get(i),"frecuencia esperada: "+ frecuenciaEsperadaExc.get(i));

        }
        

        JFreeChart grafica = ChartFactory.createBarChart("Grafica Frecuencia" , "Intervalos", "Frecuencias", data, PlotOrientation.VERTICAL, true,true,false);
        
        ChartPanel contenedor= new ChartPanel(grafica);
     
        
        JFrame ventana = new JFrame("Grafica");
        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ventana.add(contenedor);
        ventana.setSize(800,400);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(this);

    }//GEN-LAST:event_btnGraficarActionPerformed

    public void close(){
        WindowEvent winClosingEvent = new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }
    
    public void obtenerFrecuencia(){
        /*
        Inicializo y asigno los valoes correspondientes  cada variable
        */
        double intervalo = (double)1/cantIntervalos;
        double limisteInf = 0.0;
        int frecObs = 0;
        double limiteSup = intervalo;
        double res= 0.0;
        frecEsp = cantNums/cantIntervalos;
        grados = cantIntervalos - 1 ;
        sumParaChi2 = null;
        sumParaChi2 = new ArrayList<>();
        
        //Si la frecuendia esp es mayor a 5...
        if(frecEsp>= 5){
        
        //Asigno a model la tabla de frecuencias
        DefaultTableModel model = (DefaultTableModel) tablaFrec.getModel();
        String fila[] = new String[5];//Creo una lista de String
        model.setRowCount(cantIntervalos);//La cantidad de filas de la tabla va a ser igual a la cantidad de intervalos seleccionado
        model.setColumnCount(5);//la cantiad de columnas de la tabla va a ser igual a 5
        
        ArrayList <String> inters = new ArrayList<String>();//Creo vector inters de String (intervalos)
        
        String matriz[][] = new String[cantIntervalos][5];//Creo una matriz de String el cual se visualizará en la tabla
        
        //Doy tantas iteraciones como intervalos tenga
        for (int i = 0; i < cantIntervalos; i++) {
            
            //Posiciono en la primera columna los intervalos
            matriz[i][0] = df.format(limisteInf) + " - " + df.format(limiteSup);
            model.setValueAt(matriz[i][0], i, 0);
            
            intervalosParaExcel.add(matriz[i][0]);//Agrego a la lista el valor de la posición i j de la matriz
            
            //Doy tantas vueltas como cantidad de números aleatorio posea
            for (int j = 0; j < numsAleat.size(); j++) {
                
                double valor = numsAleat.get(j);//Asigno a valor un numAlet j
                
                //Con este if verifico que el valor se encuentre denro de los limites para contabilizar las frecuncias observadas
                if(valor >= limisteInf && valor < limiteSup){
                    frecObs += 1;
                }
                
      
            }
            
            frecuenciaObservadaExc.add(frecObs);//Agrego a la lista la cantidad de frec obs.
            matriz[i][1] = String.valueOf(frecObs);//Asigno el valor de la frec obs en la posicion i j de la matriz
            frecuenciaEsperadaExc.add(frecEsp);//Agrego a la lista frec obs.
            matriz[i][2] = String.valueOf(frecEsp);//Asigno el valor de la frec esp en la posicion i j de la matriz
            matriz[i][3] = String.valueOf(Math.pow((frecEsp - frecObs), 2));//misma procedimiento que la enterior
            res=Math.pow((frecEsp - frecObs), 2)/frecEsp;
            matriz[i][4] =df.format(res);//idem
            sumParaChi2.add(res);
            marcaDeClase.add((limisteInf + limiteSup)/2);
            
            //Muestro en la tabla los valores correspondientes  de cada columna
            model.setValueAt(matriz[i][1], i, 1);
            model.setValueAt(matriz[i][2], i, 2);
            model.setValueAt(matriz[i][3], i, 3);
            model.setValueAt(matriz[i][4], i, 4);
            
            //Cambio al siguiente inervalo
            limisteInf = limiteSup;
            limiteSup = limiteSup + intervalo;
            frecObs = 0;//Vuelvo a poner en cero la frec Obs para realizar nuevamentela operación
            
        }
         
       pruebaChi2(); //Realizo la prueba Chi Cuadrado
       }else{
            lblError.setText("Frec. Esperada debe ser mayor a 5");
        }
    }
    
        private void pruebaChi2() {
        double chi2calculado=0.0;
        double chi2tabulado =0.0;
        String res = "Rechaza";
        
        //Realizo la sumatoria Chicuadrado
        for (int i = 0; i < sumParaChi2.size(); i++) {
             chi2calculado+= sumParaChi2.get(i);
            
        }
        
        //Obtengo el Chi2 tabulado de acuerdo a los grados de livertad calculado
        switch(grados){
            case 4:
                chi2tabulado = 9.49;
                break;
            case 9:
                 chi2tabulado = 16.9;
                break;
            case 14:
                 chi2tabulado = 23.7;
                break;
            case 19:
                 chi2tabulado = 30.1;
                break;
            default:
                
        }
        
        //Compruebo si se acepta o no la hipótesis
        if(chi2calculado <= chi2tabulado ){
            res = "Acepta";
        }
        
        lblX2calculado.setText(df.format(chi2calculado));//Muestro el chi2 calculado
        lblXtabulado.setText(String.valueOf(chi2tabulado));//Muestro el chi2 Tabulado
        lblResultado.setText(res);//Muestro el resultado de la hipótesis

        
    }
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Punto2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Punto2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Punto2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Punto2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Punto2().setVisible(true);
            }
        });
    }

    //ATRIBUTOS DE LA INTERFAZ
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGraficar;
    private java.awt.Button bttnExcel;
    private java.awt.Button bttnGenerarNums;
    private java.awt.Button bttnVolverAtras;
    private javax.swing.ButtonGroup groupInter;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel jlblCanN;
    private javax.swing.JLabel jlblCantT;
    private javax.swing.JLabel jlblNomVentana;
    private javax.swing.JTextField jtxtCantN;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblResultado;
    private javax.swing.JLabel lblX2calculado;
    private javax.swing.JLabel lblXtabulado;
    private javax.swing.JTable num;
    private javax.swing.JRadioButton rbttn10;
    private javax.swing.JRadioButton rbttn15;
    private javax.swing.JRadioButton rbttn20;
    private javax.swing.JRadioButton rbttn5;
    private java.awt.Button salir;
    private javax.swing.JTable tablaFrec;
    // End of variables declaration//GEN-END:variables


}
