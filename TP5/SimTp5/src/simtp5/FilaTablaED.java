/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp5;

/**
 *
 * @author NICO
 */
public class FilaTablaED {
    private int id;
    private double h;
    private double tiempo;
    private double problema;
    private double dPdT;
    private double tiempoM1;
    private double problemaM1;

    public FilaTablaED() {
    }

    public FilaTablaED(int id,double h, double tiempo, double problema, double dPdT, double tiempoM1, double problemaM1) {
        this.id = id;
        this.h = h;
        this.tiempo = tiempo;
        this.problema = problema;
        this.dPdT = dPdT;
        this.tiempoM1 = tiempoM1;
        this.problemaM1 = problemaM1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
      

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }
    
    public void addTiempo() {
        this.tiempoM1 = tiempo+h ;
    }

    public double getProblema() {
        return problema;
    }

    public void setProblema(double problema) {
        this.problema = problema;
    }

    public double getdPdT() {
        return dPdT;
    }

    public void setdPdT(double dPdT) {
        this.dPdT = dPdT;
    }

    public double getTiempoM1() {
        return tiempoM1;
    }

    public void setTiempoM1(double tiempoM1) {
        this.tiempoM1 = tiempoM1;
    }

    public double getProblemaM1() {
        return problemaM1;
    }

    public void setProbM1(double p) {
        this.problemaM1 = p;
    }
    
    public void setProblemaM1() {
        this.problemaM1 = problema+(h*dPdT);
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }
    
    
    
}
