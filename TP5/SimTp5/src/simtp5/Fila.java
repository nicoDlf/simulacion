/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp5;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author NICO
 */
public class Fila {
    private int number;
    private double clk;
    private String evento;
    private int idCamion;
    private String rndTLlegada;
    private Double tiempoEntreLlegada;
    private double proximaLlegada1; 
    private double proximaLlegada2; 
    private double proximaLlegada3; 
    private double proximaLlegada4; 
    private double proximaLlegada5; 
    private double proximaLlegada6; 
    private double proximaLlegada7; 
    private double proximaLlegada8; 
    private String rndNormal1;
    private String rndNormal2;
    private String rndSupInf;
    private String supInf;
    private Double tiempoAtencion;
    private double finAtencion1;
    private double finAtencion2;
    private double finAtencion3;
    private double finAtencion4;
    private double finAtencion5;
    private double finAtencion6;
    private double finAtencion7;
    private double finAtencion8;
    private String estado1;
    private int cola1;
    private String estado2;
    private int cola2;
    private String estado3;
    private int cola3;
    private String estado4;
    private int cola4;
    private String estado5;
    private int cola5;
    private String estado6;
    private int cola6;
    private String estado7;
    private int cola7;
    private String estado8;
    private int cola8;
    private String rnd2Servicio;
    private String siNo;
    private String rndZs;
    private String zS;
    private double acumuladorTiempoInadecuado;
    private double tiempoInadecuado;
    private int totalCamionesCola;
    private int nSemana;
    private int nSemanaLimiteCamiones; 
    private String rndInterrupcion;
    private double tiempoHastaInterrupcion;
    private double inicioInterrupcion;
    private String rndProblema;
    private double problema; 
    private double tiempoInterrupcion;
    private double finInterrupcion;
    private double finServicioCamion;
   
    
    private ArrayList<Double> minTiempo;
    private double valor;

    public Fila() {
    }

  
    
    
    public Fila(int number, double clk, String evento, int idCamion, String rndTLlegada,
            Double tiempoEntreLlegada, double proximaLlegada1,  double proximaLlegada2,  
            double proximaLlegada3,  double proximaLlegada4, double proximaLlegada5,  double proximaLlegada6, 
            double proximaLlegada7, double proximaLlegada8, String rndNormal1, String rndNormal2, String rndSupInf,
            String supInf, Double tiempoAtencion, double finAtencion1, double finAtencion2, double finAtencion3, 
            double finAtencion4, double finAtencion5, Double tiempoAtencion6, double finAtencion6, double finAtencion7, 
            double finAtencion8, String estado1, int cola1, String estado2, int cola2, String estado3, int cola3, String estado4, 
            int cola4, String estado5, int cola5, String estado6, int cola6, String estado7, int cola7, String estado8, int cola8, 
            String rnd2Servicio, String siNo, String rndZs, String zS, double acumuladorTiempoCola, int totalCamionesCola, int nSemana,
            int nSemanaLimiteCamiones, String rndInterrupcion, double tiempoHastaInterrupcion, double inicioInterrupcion, String rndProblema,
            double problema, double tiempoInterrupcion, double finInterrupcion, double finServicioCamion) {
        this.number = number;
        this.clk = clk;
        this.evento = evento;
        this.idCamion = idCamion;
        this.rndTLlegada = rndTLlegada;
        this.tiempoEntreLlegada = tiempoEntreLlegada;
        this.proximaLlegada1 = proximaLlegada1;
        this.proximaLlegada2 = proximaLlegada2;
        this.proximaLlegada3 = proximaLlegada3;
        this.proximaLlegada4 = proximaLlegada4;
        this.proximaLlegada5 = proximaLlegada5;
        this.proximaLlegada6 = proximaLlegada6;
        this.proximaLlegada7 = proximaLlegada7;
        this.proximaLlegada8 = proximaLlegada8;
        this.rndNormal1 = rndNormal1;
        this.rndNormal2 = rndNormal2;
        this.rndSupInf = rndSupInf;
        this.supInf = supInf;
        this.tiempoAtencion = tiempoAtencion;
        this.finAtencion1 = finAtencion1;
        this.finAtencion2 = finAtencion2;
        this.finAtencion3 = finAtencion3;
        this.finAtencion4 = finAtencion4;
        this.finAtencion5 = finAtencion5;
        this.finAtencion6 = finAtencion6;
        this.finAtencion7 = finAtencion7;
        this.finAtencion8 = finAtencion8;
        this.estado1 = estado1;
        this.cola1 = cola1;
        this.estado2 = estado2;
        this.cola2 = cola2;
        this.estado3 = estado3;
        this.cola3 = cola3;
        this.estado4 = estado4;
        this.cola4 = cola4;
        this.estado5 = estado5;
        this.cola5 = cola5;
        this.estado6 = estado6;
        this.cola6 = cola6;
        this.estado7 = estado7;
        this.cola7 = cola7;
        this.estado8 = estado8;
        this.cola8 = cola8;
        this.rnd2Servicio = rnd2Servicio;
        this.siNo = siNo;
        this.rndZs = rndZs;
        this.zS = zS;
        this.acumuladorTiempoInadecuado = 0;
        this.totalCamionesCola = totalCamionesCola;
        this.nSemana = nSemana;
        this.nSemanaLimiteCamiones = nSemanaLimiteCamiones;
        this.rndInterrupcion = rndInterrupcion;
        this.tiempoHastaInterrupcion = tiempoHastaInterrupcion;
        this.inicioInterrupcion = inicioInterrupcion;
        this.rndProblema = rndProblema;
        this.problema = problema;
        this.tiempoInterrupcion = tiempoInterrupcion;
        this.finInterrupcion = finInterrupcion;
        this.finServicioCamion = finServicioCamion;
    }

    public double getFinServicioCamion() {
        return finServicioCamion;
    }

    public void setFinServicioCamion(double finServicioCamion) {
        this.finServicioCamion = finServicioCamion;
    }

    
    
    public void finServicioCamion() {
        this.finServicioCamion = finAtencion1-clk;
    }

    
    
    public String getRndInterrupcion() {
        return rndInterrupcion;
    }

    public void setRndInterrupcion(String rndInterrupcion) {
        this.rndInterrupcion = rndInterrupcion;
    }

    public double getTiempoHastaInterrupcion() {
        return tiempoHastaInterrupcion;
    }

    public void setTiempoHastaInterrupcion(double tiempoHastaInterrupcion) {
        this.tiempoHastaInterrupcion = tiempoHastaInterrupcion;
    }

    public double getInicioInterrupcion() {
        return inicioInterrupcion;
    }

    public void setInicioInterrupcion( ) {
        this.inicioInterrupcion = clk+tiempoHastaInterrupcion;
    }
    
     public void setInicioI(double i ) {
        this.inicioInterrupcion = i;
    }
    

    public String getRndProblema() {
        return rndProblema;
    }

    public void setRndProblema(String rndProblema) {
        this.rndProblema = rndProblema;
    }

    public double getProblema() {
        return problema;
    }

    public void setProblema(double problema) {
        this.problema = problema;
    }

    public double getTiempoInterrupcion() {
        return tiempoInterrupcion;
    }

    public void setTiempoInterrupcion(double tiempoInterrupcion) {
        this.tiempoInterrupcion = tiempoInterrupcion;
    }

    public double getFinInterrupcion() {
        return finInterrupcion;
    }

    public void setFinInterrupcion(double finInterrupcion) {
        this.finInterrupcion = finInterrupcion;
    }
    
     public void finInterrupcion() {
        this.finInterrupcion = clk+tiempoInterrupcion;
    }

    
    
    public double getTiempoInadecuado() {
        return tiempoInadecuado;
    }

    public void setTiempoInadecuado(double tiempoInadecuado) {
        this.tiempoInadecuado = tiempoInadecuado;
    }

   
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getClk() {
        return clk;
    }

    public void setClk(double clk) {
        this.clk = clk;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public int getIdCamion() {
        return idCamion;
    }

    public void setIdCamion(int idCamion) {
        this.idCamion = idCamion;
    }

    public String getRndTLlegada() {
        return rndTLlegada;
    }

    public void setRndTLlegada(String rndTLlegada) {
        this.rndTLlegada = rndTLlegada;
    }

    public Double getTiempoEntreLlegada() {
        return tiempoEntreLlegada;
    }

    public void setTiempoEntreLlegada(Double tiempoEntreLlegada) {
        this.tiempoEntreLlegada = tiempoEntreLlegada;
    }

    public double getProximaLlegada1() {
        return proximaLlegada1;
    }

    public void setProximaLlegada1(double proximaLlegada1) {
        this.proximaLlegada1 = proximaLlegada1;
    }

 
    public double getProximaLlegada2() {
        return proximaLlegada2;
    }

    public void setProximaLlegada2(double proximaLlegada2) {
        this.proximaLlegada2 = proximaLlegada2;
    }


    public double getProximaLlegada3() {
        return proximaLlegada3;
    }

    public void setProximaLlegada3(double proximaLlegada3) {
        this.proximaLlegada3 = proximaLlegada3;
    }

    public double getProximaLlegada4() {
        return proximaLlegada4;
    }

    public void setProximaLlegada4(double proximaLlegada4) {
        this.proximaLlegada4 = proximaLlegada4;
    }


    public double getProximaLlegada5() {
        return proximaLlegada5;
    }

    public void setProximaLlegada5(double proximaLlegada5) {
        this.proximaLlegada5 = proximaLlegada5;
    }


    public double getProximaLlegada6() {
        return proximaLlegada6;
    }

    public void setProximaLlegada6(double proximaLlegada6) {
        this.proximaLlegada6 = proximaLlegada6;
    }


    public double getProximaLlegada7() {
        return proximaLlegada7;
    }

    public void setProximaLlegada7(double proximaLlegada7) {
        this.proximaLlegada7 = proximaLlegada7;
    }


    public double getProximaLlegada8() {
        return proximaLlegada8;
    }

    public void setProximaLlegada8(double proximaLlegada8) {
        this.proximaLlegada8 = proximaLlegada8;
    }

    public String getRndNormal1() {
        return rndNormal1;
    }

    public void setRndNormal1(String rndNormal1) {
        this.rndNormal1 = rndNormal1;
    }

    public String getRndNormal2() {
        return rndNormal2;
    }

    public void setRndNormal2(String rndNormal2) {
        this.rndNormal2 = rndNormal2;
    }

    public String getRndSupInf() {
        return rndSupInf;
    }

    public void setRndSupInf(String rndSupInf) {
        this.rndSupInf = rndSupInf;
    }

    public String getSupInf() {
        return supInf;
    }

    public void setSupInf(String supInf) {
        this.supInf = supInf;
    }

    public Double getTiempoAtencion() {
        return tiempoAtencion;
    }

    public void setTiempoAtencion(Double tiempoAtencion) {
        this.tiempoAtencion= tiempoAtencion;
    }

    public double getFinAtencion1() {
        return finAtencion1;
    }

    public void setFinAtencion1(double finAtencion1) {
        this.finAtencion1 = finAtencion1;
    }
    
     public void reanudarAtencion1() {
        this.finAtencion1 = finServicioCamion+clk;
    }


    public double getFinAtencion2() {
        return finAtencion2;
    }

    public void setFinAtencion2(double finAtencion2) {
        this.finAtencion2 = finAtencion2;
    }

 
    public double getFinAtencion3() {
        return finAtencion3;
    }

    public void setFinAtencion3(double finAtencion3) {
        this.finAtencion3 = finAtencion3;
    }

  

    public double getFinAtencion4() {
        return finAtencion4;
    }

    public void setFinAtencion4(double finAtencion4) {
        this.finAtencion4 = finAtencion4;
    }


    public double getFinAtencion5() {
        return finAtencion5;
    }

    public void setFinAtencion5(double finAtencion5) {
        this.finAtencion5 = finAtencion5;
    }


    public double getFinAtencion6() {
        return finAtencion6;
    }

    public void setFinAtencion6(double finAtencion6) {
        this.finAtencion6 = finAtencion6;
    }


    public double getFinAtencion7() {
        return finAtencion7;
    }

    public void setFinAtencion7(double finAtencion7) {
        this.finAtencion7 = finAtencion7;
    }


    public double getFinAtencion8() {
        return finAtencion8;
    }

    public void setFinAtencion8(double finAtencion8) {
        this.finAtencion8 = finAtencion8;
    }

    public String getEstado1() {
        return estado1;
    }

    public void setEstado1(String estado1) {
        this.estado1 = estado1;
    }

    public int getCola1() {
        return cola1;
    }

    public void setCola1(int cola1) {
        this.cola1 = cola1;
    }

    public String getEstado2() {
        return estado2;
    }

    public void setEstado2(String estado2) {
        this.estado2 = estado2;
    }

    public int getCola2() {
        return cola2;
    }

    public void setCola2(int cola2) {
        this.cola2 = cola2;
    }

    public String getEstado3() {
        return estado3;
    }

    public void setEstado3(String estado3) {
        this.estado3 = estado3;
    }

    public int getCola3() {
        return cola3;
    }

    public void setCola3(int cola3) {
        this.cola3 = cola3;
    }

    public String getEstado4() {
        return estado4;
    }

    public void setEstado4(String estado4) {
        this.estado4 = estado4;
    }

    public int getCola4() {
        return cola4;
    }

    public void setCola4(int cola4) {
        this.cola4 = cola4;
    }

    public String getEstado5() {
        return estado5;
    }

    public void setEstado5(String estado5) {
        this.estado5 = estado5;
    }

    public int getCola5() {
        return cola5;
    }

    public void setCola5(int cola5) {
        this.cola5 = cola5;
    }

    public String getEstado6() {
        return estado6;
    }

    public void setEstado6(String estado6) {
        this.estado6 = estado6;
    }

    public int getCola6() {
        return cola6;
    }

    public void setCola6(int cola6) {
        this.cola6 = cola6;
    }

    public String getEstado7() {
        return estado7;
    }

    public void setEstado7(String estado7) {
        this.estado7 = estado7;
    }

    public int getCola7() {
        return cola7;
    }

    public void setCola7(int cola7) {
        this.cola7 = cola7;
    }

    public String getEstado8() {
        return estado8;
    }

    public void setEstado8(String estado8) {
        this.estado8 = estado8;
    }

    public int getCola8() {
        return cola8;
    }

    public void setCola8(int cola8) {
        this.cola8 = cola8;
    }

    public String getRnd2Servicio() {
        return rnd2Servicio;
    }

    public void setRnd2Servicio(String rnd2Servicio) {
        this.rnd2Servicio = rnd2Servicio;
    }

    public String getSiNo() {
        return siNo;
    }

    public void setSiNo(String siNo) {
        this.siNo = siNo;
    }

    public String getRndZs() {
        return rndZs;
    }

    public void setRndZs(String rndZs) {
        this.rndZs = rndZs;
    }

    public String getzS() {
        return zS;
    }

    public void setzS(String zS) {
        this.zS = zS;
    }

    public double getAcumuladorTiempoInadecuado() {
        return acumuladorTiempoInadecuado;
    }

    public void setAcumuladorTiempoInadecuado(double acumuladorTiempoInadecuado) {
        this.acumuladorTiempoInadecuado = acumuladorTiempoInadecuado;
    }

    public int getTotalCamionesCola() {
        return totalCamionesCola;
    }

    public void setTotalCamionesCola(int totalCamionesCola) {
        this.totalCamionesCola = totalCamionesCola;
    }
   

    public int getnSemana() {
        return nSemana;
    }

    public void setnSemana(int nSemana) {
        this.nSemana = nSemana;
    }

    public int getnSemanaLimiteCamiones() {
        return nSemanaLimiteCamiones;
    }

    public void setnSemanaLimiteCamiones(int nSemanaLimiteCamiones) {
        this.nSemanaLimiteCamiones = nSemanaLimiteCamiones;
    }
    
        
    public double tiempoMinimo(){
          if(minTiempo==null){
          minTiempo= new ArrayList<>();
        }
        minTiempo.clear();
        
        if(proximaLlegada1!=0){
            minTiempo.add(proximaLlegada1);
        }
        if(proximaLlegada2!=0){
            minTiempo.add(proximaLlegada2);
        }
        if(proximaLlegada3!=0){
            minTiempo.add(proximaLlegada3);
        }
        if(proximaLlegada4!=0){
            minTiempo.add(proximaLlegada4);
        }
        if(proximaLlegada5!=0){
            minTiempo.add(proximaLlegada5);
        }
        if(proximaLlegada6!=0){
            minTiempo.add(proximaLlegada6);
        }
        if(proximaLlegada7!=0){
            minTiempo.add(proximaLlegada7);
        }
        if(proximaLlegada8!=0){
            minTiempo.add(proximaLlegada8);
        }
        
        if(finAtencion1!=0){
            minTiempo.add(finAtencion1);
        }
       
        if(finAtencion2!=0){
            minTiempo.add(finAtencion2);
        }
        if(finAtencion3!=0){
            minTiempo.add(finAtencion3);
        }
        if(finAtencion4!=0){
            minTiempo.add(finAtencion4);
        }
        if(finAtencion5!=0){
            minTiempo.add(finAtencion5);
        }
        if(finAtencion6!=0){
            minTiempo.add(finAtencion6);
        }
        if(finAtencion7!=0){
            minTiempo.add(finAtencion7);
        }
        if(finAtencion8!=0){
            minTiempo.add(finAtencion8);
        }
        
        if(inicioInterrupcion!=0){
            minTiempo.add(inicioInterrupcion);
        }
        
        if(finInterrupcion!=0){
             minTiempo.add(finInterrupcion);
        }
        
 
          
        valor = Collections.min(minTiempo);
               
        return valor;
    }
    
       
     public double proximaLl(){
        return clk+tiempoEntreLlegada;
    }
   
     public double proximaFA(){
        return clk + tiempoAtencion;
    }
     
     public void cola1Add(){
         cola1++;
     }
     
      public void cola1Remove(){
         cola1--;
     }
     
     public void cola2Add(){
         cola2++;
     }
     
      public void cola1Remove2(){
         cola2--;
     }
     
     public void cola3Add(){
         cola3++;
     }
     
      public void cola1Remove3(){
         cola3--;
     }
     
     public void cola4Add(){
         cola4++;
     }
     
      public void cola1Remove4(){
         cola4--;
     }
     
     public void cola5Add(){
         cola5++;
     }
     
      public void cola1Remove5(){
         cola5--;
     }
     
     public void cola6Add(){
         cola6++;
     }
     
      public void cola1Remove6(){
         cola6--;
     }
     
     public void cola7Add(){
         cola7++;
     }
     
      public void cola1Remove7(){
         cola7--;
     }
     
     public void cola8Add(){
         cola8++;
     }
     
      public void cola1Remove8(){
         cola8--;
     }
     
     public int colaTotal(){
         totalCamionesCola = (cola1+cola2+cola3+cola4+cola5+cola6+cola7+cola8);
         return totalCamionesCola;
     }
     
     public void aumentarAcumuladorTiempoInadecuado(double aumento){
         
          this.acumuladorTiempoInadecuado += aumento;
     }
     
     public double tiempoEnCola(double tiempo){
         return (clk-tiempo);
     }
    
     public void addSemana(){
         this.nSemana++;
     }
     
 
}
