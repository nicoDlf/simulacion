/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp5;

/**
 *
 * @author NICO
 */
public class Camion {
    private int id;
    private String estado;
    private int estadoC;
    private int enZonaServicio;
    private String realizoSegServicio;
    private int realizoSS;
    

    public Camion() {
    }

    public Camion(int id, String estado, int estadoC, String realizoSegServicio, int realizoSS, int enZonaServicio) {
        this.id = id;
        this.estado = estado;
        this.estadoC = estadoC;
        this.realizoSegServicio = realizoSegServicio;
        this.realizoSS = realizoSS;
        this.enZonaServicio = enZonaServicio;
    }

    public int getEnZonaServicio() {
        return enZonaServicio;
    }

    public void setEnZonaServicio(int enZonaServicio) {
        this.enZonaServicio = enZonaServicio;
    }

    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getEstadoC() {
        return estadoC;
    }

    public void setEstadoC(int estadoC) {
          
        this.estadoC = estadoC;   
        if(estadoC==1){
            this.estado="SA";
        }else{
            this.estado="EA"; 
        }
   
        
    }

    public String getRealizoSegServicio() {
        return realizoSegServicio;
    }

    public void setRealizoSegServicio(String realizoSegServicio) {
        this.realizoSegServicio = realizoSegServicio;
    }

    public int getRealizoSS() {
        return realizoSS;
    }

    public void setRealizoSS(int realizoSS) {
        this.realizoSS = realizoSS;
         if(realizoSS==1){
            this.realizoSegServicio="SI";
        }else{
            this.realizoSegServicio="NO"; 
        }
    }

    
   
    
    
}
