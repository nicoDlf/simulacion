/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simtp5;

/**
 *
 * @author NICO
 */
public class IndiceLLegada {
    double indice1;
    double indice2;
    double indice3;
    double indice4;
    double indice5;
    double indice6;
    double indice7;
    double indice8;
    double indiceInterrupcion;

    public IndiceLLegada() {
        
        this.indice1 = 0.43;
         this.indice2 = 0.16;
          this.indice3 = 0.14;
           this.indice4 = 0.10;
            this.indice5 = 0.09;
             this.indice6 = 0.08;
              this.indice7 = 0.05;
               this.indice8 = 0.05;
               this.indiceInterrupcion = 1.2;
               
    }
    
    public void aumentarIndice(){
         this.indice1 = indice1*1.0024;
         this.indice2 = indice2*1.0024;
          this.indice3 = indice3*1.0024;
           this.indice4 = indice4*1.0024;
            this.indice5 = indice5*1.0024;
             this.indice6 = indice6*1.0024;
              this.indice7 = indice7*1.0024;
               this.indice8 = indice8*1.0024;
    }

    public double getIndice1() {
        return indice1;
    }

    public double getIndice2() {
        return indice2;
    }

    public double getIndice3() {
        return indice3;
    }

    public double getIndice4() {
        return indice4;
    }

    public double getIndice5() {
        return indice5;
    }

    public double getIndice6() {
        return indice6;
    }

    public double getIndice7() {
        return indice7;
    }

    public double getIndice8() {
        return indice8;
    }

    public double getIndiceInterrupcion() {
        return indiceInterrupcion;
    }
    
    
    
    
}
