/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author NICO
 */
public class Fila {
    private int number;
    private double clk;
    private String evento;
    private String rndTLlegada;
    private Double tiempoEntreLlegada;
    private double proximaLlegada; 
    private String rndPeluqero;
    private String Peluquero;
    private String rndTiempoAtencion;
    private Double tiempoAtencion;
    private double finAtencionAP;
    private double finAtencionVA;
    private double finAtencionVB;
    private String estadoAP;
    private int colaAP;
    private String estadoVA;
    private int colaVA;
    private String estadoVB;
    private int colaVB;
    private int contadorClientesRetiran;
    private Double acumuladorPlataAP;
    private Double acumuladorPlataVA;
    private Double acumuladorPlataVB;
    private Double contadorDias;
    private Double acumuladorPlataTotalAP;
    private Double acumuladorPlataTotalVA;
    private Double acumuladorPlataTotalVB;
    
    private ArrayList<Double> minTiempo;
    private double valor;

    public Fila(int number, double clk, String evento, String rndTLlegada, 
            Double tiempoEntreLlegada, double proximaLlegada, String rndPeluqero, 
            String Peluquero, String rndTiempoAtencion, Double tiempoAtencion, 
            double finAtencionAP, double finAtencionVA, double finAtencionVB, 
            String estadoAP, int colaAP, String estadoVA, int colaVA, String estadoVB, 
            int colaVB, int contadorClientesRetiran, Double acumuladorPlataAP, 
            Double acumuladorPlataVA, Double acumuladorPlataVB,Double contadorDias, 
            Double acumuladorPlataTotalAP,Double acumuladorPlataTotalVA,Double acumuladorPlataTotalVB) {
        this.number = number;
        this.clk = clk;
        this.evento = evento;
        this.rndTLlegada = rndTLlegada;
        this.tiempoEntreLlegada = tiempoEntreLlegada;
        this.proximaLlegada = proximaLlegada;
        this.rndPeluqero = rndPeluqero;
        this.Peluquero = Peluquero;
        this.rndTiempoAtencion = rndTiempoAtencion;
        this.tiempoAtencion = tiempoAtencion;
        this.finAtencionAP = finAtencionAP;
        this.finAtencionVA = finAtencionVA;
        this.finAtencionVB = finAtencionVB;
        this.estadoAP = estadoAP;
        this.colaAP = colaAP;
        this.estadoVA = estadoVA;
        this.colaVA = colaVA;
        this.estadoVB = estadoVB;
        this.colaVB = colaVB;
        this.contadorClientesRetiran = contadorClientesRetiran;
        this.acumuladorPlataAP = acumuladorPlataAP;
        this.acumuladorPlataVA = acumuladorPlataVA;
        this.acumuladorPlataVB = acumuladorPlataVB;
        this.contadorDias = contadorDias;
        this.acumuladorPlataTotalAP = acumuladorPlataTotalAP;
        this.acumuladorPlataTotalVA = acumuladorPlataTotalVA;
        this.acumuladorPlataTotalVB = acumuladorPlataTotalVB;
        
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getClk() {
        return clk;
    }

    public void setClk(double clk) {
        this.clk = clk;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public String getRndTLlegada() {
        return rndTLlegada;
    }

    public void setRndTLlegada(String rndTLlegada) {
        this.rndTLlegada = rndTLlegada;
    }

    public Double getTiempoEntreLlegada() {
        return tiempoEntreLlegada;
    }

    public void setTiempoEntreLlegada(Double tiempoEntreLlegada) {
        this.tiempoEntreLlegada = tiempoEntreLlegada;
    }

    public double getProximaLlegada() {
        return proximaLlegada;
    }

    public void setProximaLlegada(double proximaLlegada) {
        this.proximaLlegada = proximaLlegada;
    }
    
    public void calcularProximaLlegada(){
        this.proximaLlegada = tiempoEntreLlegada + clk;
    }

    public String getRndPeluqero() {
        return rndPeluqero;
    }

    public void setRndPeluqero(String rndPeluqero) {
        this.rndPeluqero = rndPeluqero;
    }

    public String getPeluquero() {
        return Peluquero;
    }

    public void setPeluquero(String Peluquero) {
        this.Peluquero = Peluquero;
    }

    public String getRndTiempoAtencion() {
        return rndTiempoAtencion;
    }

    public void setRndTiempoAtencion(String rndTiempoAtencion) {
        this.rndTiempoAtencion = rndTiempoAtencion;
    }

    public Double getTiempoAtencion() {
        return tiempoAtencion;
    }

    public void setTiempoAtencion(Double tiempoAtencion) {
        this.tiempoAtencion = tiempoAtencion;
    }

    public double getFinAtencionAP() {
        return finAtencionAP;
    }

    public void setFinAtencionAP(double finAtencionAP) {
        this.finAtencionAP = finAtencionAP;
    }
    
    public void calcularFinAtencionAP(){
        this.finAtencionAP = tiempoAtencion+clk;
    }

    public double getFinAtencionVA() {
        return finAtencionVA;
    }

    public void setFinAtencionVA(double finAtencionVA) {
        this.finAtencionVA = finAtencionVA;
    }
    
    public void calcularFinAtencionVA(){
        this.finAtencionVA = tiempoAtencion+clk;
    }

    public double getFinAtencionVB() {
        return finAtencionVB;
    }

    public void setFinAtencionVB(double finAtencionVB) {
        this.finAtencionVB = finAtencionVB;
    }
    
    public void calcularFinAtencionVB(){
        this.finAtencionVB = tiempoAtencion+clk;
    }

    public String getEstadoAP() {
        return estadoAP;
    }

    public void setEstadoAP(String estadoAP) {
        this.estadoAP = estadoAP;
    }

    public int getColaAP() {
        return colaAP;
    }

    public void setColaAP(int colaAP) {
        this.colaAP = colaAP;
    }

    public void addColaAP(){
        this.colaAP++;
    }
    
    public void removeColaAP(){
        this.colaAP--;
    }
    
    public String getEstadoVA() {
        return estadoVA;
    }

    public void setEstadoVA(String estadoVA) {
        this.estadoVA = estadoVA;
    }

    public int getColaVA() {
        return colaVA;
    }

    public void setColaVA(int colaVA) {
        this.colaVA = colaVA;
    }
    
      public void addColaVA(){
        this.colaVA++;
    }
    
    public void removeColaVA(){
        this.colaVA--;
    }

    public String getEstadoVB() {
        return estadoVB;
    }

    public void setEstadoVB(String estadoVB) {
        this.estadoVB = estadoVB;
    }

    public int getColaVB() {
        return colaVB;
    }

    public void setColaVB(int colaVB) {
        this.colaVB = colaVB;
    }
    
    public void addColaVB(){
        this.colaVB++;
    }
    
    public void removeColaVB(){
        this.colaVB--;
    }

    public int getContadorClientesRetiran() {
        return contadorClientesRetiran;
    }

    public void setContadorClientesRetiran(int contadorClientesRetiran) {
        this.contadorClientesRetiran = contadorClientesRetiran;
    }
    
    public void addClientesRetiran(){
        contadorClientesRetiran++;
    }

    public Double getAcumuladorPlataAP() {
        return acumuladorPlataAP;
    }

    public void setAcumuladorPlataAP(Double acumuladorPlataAP) {
        this.acumuladorPlataAP = acumuladorPlataAP;
    }
    
    public void gananciaAP(){
        this.acumuladorPlataAP +=3;
    }

    public Double getAcumuladorPlataVA() {
        return acumuladorPlataVA;
    }

    public void setAcumuladorPlataVA(Double acumuladorPlataVA) {
        this.acumuladorPlataVA = acumuladorPlataVA;
    }
    
    public void gananciaVA(){
        this.acumuladorPlataVA +=5;
    }

    public Double getAcumuladorPlataVB() {
        return acumuladorPlataVB;
    }

    public void setAcumuladorPlataVB(Double acumuladorPlataVB) {
        this.acumuladorPlataVB = acumuladorPlataVB;
    }

    public void gananciaVB(){
        this.acumuladorPlataVB +=5;
    }
     

    public Double getContadorDias() {
        return contadorDias;
    }

    public void setContadorDias(Double contadorDias) {
        this.contadorDias = contadorDias;
    }
    
    public void addDia(){
        this.contadorDias++;
    }
    
        
     public double tiempoMinimo(){
          if(minTiempo==null){
          minTiempo= new ArrayList<>();
        }
        minTiempo.clear();
        
        if(proximaLlegada!=0){
            minTiempo.add(proximaLlegada);
        }
        
        if(finAtencionAP!=0){
            minTiempo.add(finAtencionAP);
        }
       
        if(finAtencionVA!=0){
            minTiempo.add(finAtencionVA);
        }
        if(finAtencionVB!=0){
            minTiempo.add(finAtencionVB);
        }
     
   
        valor = Collections.min(minTiempo);
               
        return valor;
    }

    public Double getAcumuladorPlataTotalAP() {
        return acumuladorPlataTotalAP;
    }

    public void setAcumuladorPlataTotalAP() {
        this.acumuladorPlataTotalAP += acumuladorPlataAP;
    }
    
    

    public Double getAcumuladorPlataTotalVA() {
        return acumuladorPlataTotalVA;
    }

    public void setAcumuladorPlataTotalVA() {
        this.acumuladorPlataTotalVA += acumuladorPlataVA;
    }

    public Double getAcumuladorPlataTotalVB() {
        return acumuladorPlataTotalVB;
    }

    public void setAcumuladorPlataTotalVB() {
        this.acumuladorPlataTotalVB += acumuladorPlataVB;
    }
     
    
     
    
}
