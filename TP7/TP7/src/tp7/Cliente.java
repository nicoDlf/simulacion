/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7;

/**
 *
 * @author NICO
 */
public class Cliente {
    private int numero;
    private String estado;
    private Double tiempoEntroCola;
    private String cola;

    public Cliente(int numero, String estado, Double tiempoEntroCola, String cola) {
        this.numero = numero;
        this.estado = estado;
        this.tiempoEntroCola = tiempoEntroCola;
        this.cola = cola;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getTiempoEntroCola() {
        return tiempoEntroCola;
    }

    public void setTiempoEntroCola(Double tiempoEntroCola) {
        this.tiempoEntroCola = tiempoEntroCola;
    }

    public String getCola() {
        return cola;
    }

    public void setCola(String cola) {
        this.cola = cola;
    }
    
    
}
